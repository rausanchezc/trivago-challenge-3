<?php
/**
 * Created by PhpStorm.
 * User: rsanchez
 * Date: 12/02/2016
 * Time: 19:33
 */

namespace AnalyzerBundle\DataFixtures\ORM;


use AnalyzerBundle\Entity\Review;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SplFileObject;
use Symfony\Component\Finder\Finder;

class LoadReviewData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $finder = new Finder();
        $finder->files()->in('src/AnalyzerBundle/Resources/datasets')->name('reviews.csv');

        foreach($finder as $file)
        {
            $filename = $file->getRealPath();

            $csv_file = new SplFileObject($filename);

            $reader = new CsvReader($csv_file);
            $reader->setHeaderRowNumber(0);
            $reader->setStrict(false);

            foreach($reader as $row)
            {
                $review = new Review();

                $review->setCorpus($row['Review']);
                $review->setHotelId($row['HotelId']);

                $manager->persist($review);
                $manager->flush();
            }
        }
    }
}