<?php
/**
 * Created by PhpStorm.
 * User: rsanchez
 * Date: 12/02/2016
 * Time: 19:33
 */

namespace AnalyzerBundle\DataFixtures\ORM;


use AnalyzerBundle\Entity\Qualifier;
use AnalyzerBundle\Entity\Topic;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SplFileObject;
use Symfony\Component\Finder\Finder;

class LoadCriteriaData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $finder = new Finder();
        $finder->files()->in('src/AnalyzerBundle/Resources/datasets')->name('criteria.csv');

        foreach($finder as $file)
        {
            $filename = $file->getRealPath();

            $csv_file = new SplFileObject($filename);

            $reader = new CsvReader($csv_file);
            $reader->setHeaderRowNumber(0);
            $reader->setStrict(false);

            $main = new Topic();

            foreach($reader as $row)
            {
                // Topic
                if('' != $row['Topic'])
                {
                    $main = new Topic();
                    $main->setName($row['Topic']);

                    $manager->persist($main);
                    $manager->flush();

                }

                // Alternate Name
                if('' != $row['Alternate Name'])
                {
                    $alt = new Topic();
                    $alt->setName($row['Alternate Name']);
                    $alt->setMainKey($main);


                    $manager->persist($alt);
                    $manager->flush();
                }

                // Positives
                if('' != $row['Positives'])
                {
                    $positive = new Qualifier();

                    $positive->setType('POSITIVE');
                    $positive->setName($row['Positives']);
                    $positive->setWeight(1);

                    $manager->persist($positive);
                    $manager->flush();
                }

                // Negatives
                if('' != $row['Negatives'])
                {
                    $positive = new Qualifier();

                    $positive->setType('NEGATIVE');
                    $positive->setName($row['Negatives']);
                    $positive->setWeight(-1);

                    $manager->persist($positive);
                    $manager->flush();
                }

            }
        }
    }
}