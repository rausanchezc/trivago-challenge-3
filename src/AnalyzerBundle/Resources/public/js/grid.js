$(document).ready(function() {

    // JQuery Grid

    var grid = $("#grid-data").bootgrid(
    {
        ajax: true,
        ajaxSettings:{
            method: "GET",
            cache: false
        },
        url: "http://localhost:8000/custom-reviews",
        requestHandler: function(request) {

            // Inject parameters

            var filterId            = $("#id-filter").val();
            var filterContent       = $("#content-filter").val();
            var filterScore         = $("#score-filter").val();
            var filterTotalScore    = $("#total-score-filter").val();


            if(filterId != "") {
                request.filterId    = filterId;
                request.predicateId = $("#predicate-id-filter").val();
            }

            if(filterContent != ""){
                request.filterContent    = filterContent;
                request.predicateContent = $("#predicate-content-filter").val();
            }

            if(filterScore != ""){
                request.filterScore     = filterScore;
                request.predicateScore  = $("#predicate-score-filter").val();
            }

            if(filterTotalScore != ""){
                request.filterTotalScore    = filterTotalScore;
                request.predicateTotalScore = $("#predicate-total-score-filter").val();
            }

            return request;
        },
        templates:{
            search: "",
            actions:""
        },
        converters: {
            "scores": {
                to: function(scores)
                {
                    var allScores = "";

                    for(var i = 0 ; i < scores.length; i++)
                    {
                        var topic = scores[i].topic != null ? scores[i].topic : "";
                        var qualifier = scores[i].qualifier !== null ? scores[i].qualifier : "";

                        allScores +=  topic +" "+ qualifier;
                        if(i < scores.length - 1){ allScores += ","; }
                    }

                    return allScores;
                }
            }
        },
        formatters: {
            "actions": function(colum, row){
                return "<button type=\"button\" class=\"btn btn-xs btn-default action-delete\" data-row-id=\"" + row.id + "\"><span class=\"glyphicon glyphicon-trash\"></span></button>";
            }
        }
    }).on("loaded.rs.jquery.bootgrid" , function(){

            // Delete action behavior
            grid.find(".action-delete").on("click", function(e)
            {
                var id = $(this).data("row-id");

                $.ajax({
                    url : "http://localhost:8000/delete-review/" + id ,
                    type: 'DELETE',
                    success: function(){
                        $("#grid-data").bootgrid("reload");
                    },
                    error: function(){
                        console.log("Something was wrong!");
                    }
                });

            });

            // Filters behavior
            var searchTimeout = null;
            var maxTimeout = 500;


            // Handler for filter inputs
            $(".form").find('input').on('input', function(){
                clearTimeout(searchTimeout);
                searchTimeout = setTimeout(function(){
                    $('#grid-data').bootgrid('reload');
                }, maxTimeout);
            });

            // Handler for filter selects
            $(".form").find('select').on('change', function(){
                clearTimeout(searchTimeout);
                searchTimeout = setTimeout(function(){
                    $('#grid-data').bootgrid('reload');
                }, maxTimeout);
            });


            // Reset button behavior
            $("#reset-filters-button").on("click", function(){
                $("input").val("");
                $('#grid-data').bootgrid('reload');
            });

            // Run analysis behavior
            $("#run-analysis-button").on("click", function(){

                $.post("http://localhost:8000/analyze-reviews", function(res){
                    $('#grid-data').bootgrid('reload');
                });

            });

        });

});