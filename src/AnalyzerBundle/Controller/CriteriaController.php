<?php

namespace AnalyzerBundle\Controller;

use AnalyzerBundle\Entity\Qualifier;
use AnalyzerBundle\Entity\Topic;
use AnalyzerBundle\Form\Type\QualifierType;
use AnalyzerBundle\Form\Type\TopicType;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class CriteriaController extends Controller
{
    private $em;
    private $templating;
    private $formFactory;
    private $router;

    /**
     * CriteriaController constructor.
     *
     * @param EntityManager $entityManager
     * @param $templating
     * @param $router
     * @param $formFactory
     * @param $logger
     */
    public function __construct(EntityManager $entityManager, $templating, $router, $formFactory, $logger)
    {
        $this->em = $entityManager;
        $this->templating = $templating;
        $this->formFactory = $formFactory;
        $this->router = $router;
        $this->logger = $logger;
    }


    /**
     * Get all Criteria stored into database
     *
     * @return Response
     */
    public function getAllCriteriaAction()
    {
        $topics = $this->em->getRepository("AnalyzerBundle:Topic")->findAll();

        $qualifiers = $this->em->getRepository("AnalyzerBundle:Qualifier")->findBy(array(),array('name'=>'ASC'));

        return $this->templating->renderResponse('AnalyzerBundle:Criteria:list.html.twig',
            array('topics' => $topics, 'qualifiers' => $qualifiers));
    }


    /**
     * Create new Topic
     *
     * @param Request $request
     * @return Response
     */
    public function addTopicAction(Request $request)
    {
        $this->logger->debug('Add new topic Action');

        $topic = new Topic();

        $topicForm = $this->formFactory->create( TopicType::class , $topic);

        $topicForm->handleRequest($request);

        if ($topicForm->isSubmitted() && $topicForm->isValid()) {

            $this->em->persist($topic);
            $this->em->flush();

            return new RedirectResponse($this->router->generate('list_criteria'));

        }

        return $this->templating->renderResponse('AnalyzerBundle:Criteria:addTopic.html.twig',
            array('addTopicForm'=>$topicForm->createView()));
    }

    /**
     * Create new Qualifier
     *
     * @param Request $request
     * @return Response
     */
    public function addQualifierAction(Request $request)
    {
        $this->logger->debug('Add new qualifier Action');

        $qualifier = new Qualifier();

        $qualifierForm = $this->formFactory->create(QualifierType::class, $qualifier);

        $qualifierForm->handleRequest($request);

        if($qualifierForm->isSubmitted() && $qualifierForm->isValid()){

            $this->em->persist($qualifier);
            $this->em->flush();

            return new RedirectResponse($this->router->generate('list_criteria'));
        }

        return $this->templating->renderResponse('AnalyzerBundle:Criteria:addQualifier.html.twig',
            array('addQualifierForm'=> $qualifierForm->createView()));
    }
}
