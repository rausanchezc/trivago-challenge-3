<?php

namespace AnalyzerBundle\Controller;

use AnalyzerBundle\Entity\Review;
use AnalyzerBundle\Form\Type\ReviewType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ReviewController extends Controller
{

    /**
     * Create new Review for and hotel ID
     *
     * @param Request $request
     * @return Response
     */
    public function addReviewAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $review = new Review();

        $reviewForm = $this->createForm(ReviewType::class, $review);

        $reviewForm->handleRequest($request);

        if ($reviewForm->isSubmitted() && $reviewForm->isValid()) {

            $em->persist($review);
            $em->flush();

            return $this->redirectToRoute('grid_reviews');
        }

        return $this->render('AnalyzerBundle:Review:add.html.twig',
            array('reviewForm'=> $reviewForm->createView()));
    }


    /**
     * Delete Review
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteReviewAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $review = $em->getRepository("AnalyzerBundle:Review")->find($id);

        if(!$review){
            throw $this->createNotFoundException('No review found with id:'.$id);
        }

        $em->remove($review);
        $em->flush();

        return new JsonResponse(
            array(
                "status"=> "OK",
                "message" => "Review with id '".$id."' was deleted correctly"));
    }


    /**
     * Upload single CSV file
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function importReviewsAction(Request $request)
    {
        $review_service = $this->get('analyzer.service.review');

        $uploaderForm = $this->createFormBuilder()
            ->add('file', FileType::class,
                array(
                    'label' => 'File',
                    'mapped' => false
                ))
            ->add('save', SubmitType::class,
                array('label' => 'Upload'))
            ->getForm();


        $result = array('reviews' => [], 'numNewReviews' => 0);

        $uploaderForm->handleRequest($request);

        if($uploaderForm->isValid() && $uploaderForm->isSubmitted())
        {
            $raw_file = $uploaderForm['file']->getData();

            $result = $review_service->importCSVReviewFile($raw_file);

        }


        return $this->render('AnalyzerBundle:Review:upload.html.twig',
            array(
                'uploaderForm' => $uploaderForm->createView(),
                'reviews' => $result['reviews'],
                'total' => count($result['reviews']),
                'numNewReviews' => $result['numNewReviews']
            ));
    }
}
