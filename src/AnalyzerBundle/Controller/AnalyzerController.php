<?php

namespace AnalyzerBundle\Controller;

use Doctrine\ORM\Query\Expr;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class AnalyzerController extends Controller
{

    /**
     * Get reviews by applying custom filters.
     *
     * @param Request $request
     * @return Response
     */
    public function getCustomReviewsAction(Request $request)
    {
        // Get Parameters

        $params = [];

        $params['filterId']         = $request->query->get("filterId");
        $params['predicateId']      = $request->query->get("predicateId");


        $params['filterContent']    = $request->query->get("filterContent");
        $params['predicateContent'] = $request->query->get("predicateContent");


        $params['filterScore']          = $request->query->get('filterScore');
        $params['predicateScore']       = $request->query->get('predicateScore');

        
        $params['filterTotalScore']     = $request->query->get("filterTotalScore");
        $params['predicateTotalScore']  = $request->query->get("predicateTotalScore");


        // Retrieve Reviews
        $reviews = $this->getDoctrine()
            ->getRepository("AnalyzerBundle:Review")
            ->findByCustomFilters($params);

        // TODO improve serialization method | http://thomas.jarrand.fr/blog/serialization/
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $scoreSerializer = function ($scores) {
            $serialized = [];

            foreach ($scores as $score) {
                $serialized [] = array(
                    "topic" => $score->getTopic() ? $score->getTopic()->getName() : null,
                    "qualifier" => $score->getQualifier()->getName(),
                    "count" => $score->getCount()
                );
            }

            return $serialized;
        };

        $dateSerializer = function ($datetime) {
            return $datetime instanceof \DateTime
                ? $datetime->getTimestamp()
                : '';
        };

        $normalizer->setCallbacks(array(
            'scores'                => $scoreSerializer,
            'creationDate'          => $dateSerializer,
            'lastModificationDate'  => $dateSerializer
        ));


        $encoders = array($encoder);
        $normalizers = array($normalizer);

        $serializer = new Serializer($normalizers, $encoders);


        $response = new Response();

        $content = array(
            "current" => 1,
            "rowCount" => 10,
            'rows' => $reviews,
            "total" => count($reviews)
        );

        // Build Response
        $response->setContent($serializer->serialize($content, 'json'));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    /**
     *  Analyze all pending reviews
     *
     * @return JsonResponse
     */
    public function analyzeReviewsAction()
    {
        $review_service = $this->get('analyzer.service.review');

        $status = 200; $message = ""; $analyzed = 0;

        try
        {
            $analyzed = $review_service->analyzeAllReviews();

            $message = "Number of analyzed reviews: ".$analyzed ;

        }catch(Exception $e)
        {
            $status = 500;
            $message = "Internal Error ".$e->getCode();
        }

        return new JsonResponse(
            array(
                "status"    => $status,
                "message"   => $message
                )
        );
    }
}
