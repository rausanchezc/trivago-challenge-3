<?php


namespace AnalyzerBundle\Services;


use AnalyzerBundle\Entity\Review;
use AnalyzerBundle\Entity\Score;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NoResultException;
use Ddeboer\DataImport\Reader\CsvReader;
use SplFileObject;

class ReviewService
{

    private $em;

    /**
     * ReviewService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    /**
     * Get all reviews
     *
     * @return array
     */
    public function getAllReviews()
    {
        $query = $this->em->createQuery(
            'SELECT r FROM AnalyzerBundle:Review r LEFT JOIN r.scores s'
        );

        return $query->getResult();
    }


    /**
     * Analyze all reviews
     *
     */
    public function analyzeAllReviews()
    {

        // Get only not analyzed Reviews
        $reviews = $this->em->getRepository("AnalyzerBundle:Review")->findAllNotAnalyzed();

        // Get all Qualifiers
        $qualifiers = $this->em->getRepository("AnalyzerBundle:Qualifier")->findAll();

        // Topic Cache
        $cached_topics = [];

        foreach ($reviews as $review)
        {
            // Prepare review text
            $review_text = $this->prepareReviewText($review->getCorpus());
            $matched_qualifiers = array();
            $totalScore = 0;

            // Extract lines
            $lines = preg_split('/[.,]/', $review_text);


            foreach ($lines as $line)
            {
                // Qualifier extraction
                foreach ($qualifiers as $qualifier)
                {
                    $key = $qualifier->getName();

                    if (preg_match_all('/\b'. $key .'\b/i', $line, $m))
                    {
                        if(!isset($matched_qualifiers[$key]))
                        {
                            $matched_qualifiers [$key] = array( "qualifier" => $qualifier, "topics" => array());
                        }

                        // find topics related
                        $words = explode(' ', $line);

                        foreach($words as $word)
                        {
                            $word = trim($word);

                            $topic = null;

                            if(isset($cached_topics[$word]))
                            {
                                $topic = $cached_topics[$word];
                            }
                            else{
                                $topic = $this->em->getRepository("AnalyzerBundle:Topic")->findOneByName($word);
                            }

                            if(null == $topic)
                            {
                                $root = preg_split('/(es)$ | (ies)$ | (s)$/x', $word);

                                $topic = $this->em->getRepository("AnalyzerBundle:Topic")->findOneByName($root[0]);
                            }

                            if(null != $topic)
                            {
                                $matched_qualifiers[$key]["topics"][] = $topic;

                                // update cache
                                $cached_topics [$topic->getName()] = $topic;
                            }
                        }
                    }
                }
            }

            // Build all scores related with this review
            foreach($matched_qualifiers as $value)
            {
                if(count($value["topics"]) > 0)
                {
                    foreach($value["topics"] as $topic)
                    {
                        $score = new Score();

                        $qualifier = $value["qualifier"];

                        $score->setReview($review);
                        $score->setQualifier($qualifier);
                        $score->setCount(0);
                        $score->setTopic($topic);
                        $totalScore += + $qualifier->getWeight();

                        $this->em->persist($score);
                    }
                }
                else {

                    $score = new Score();

                    $score->setReview($review);
                    $score->setQualifier($value["qualifier"]);
                    $score->setCount(0);
                    $totalScore += + $qualifier->getWeight();

                    $this->em->persist($score);
                }
            }

            // Update total score
            $review->setTotalScore($totalScore);
            $review->setLastCalculationDate(new \DateTime());
            $this->em->persist($review);
            $this->em->flush();

        }

        return count($reviews);
    }


    /**
     *  Import Reviews from CSV File
     *
     * @param $raw_file
     * @return array['reviews'] contain each review row processed, array['numNewReviews] number of new reviews
     */
    public function importCSVReviewFile($raw_file)
    {

        $reviews = [];
        $numNewReviews = 0;

        // Parse raw file to CSV file
        $csv_file = new SplFileObject($raw_file->getPathName());
        $reader = new CsvReader($csv_file);

        // Configure reader
        $reader->setHeaderRowNumber(0);
        $reader->setStrict(false);

        // Process each row of CSV file
        foreach($reader as $row)
        {
            $reviews[] = $row;

            // Create Review object
            $review = new Review();
            $review->setCorpus($row['Review']);
            $review->setHotelId($row['HotelId']);


            // Check if 'Review' already exist
            $stored_review = $this->em
                ->getRepository('AnalyzerBundle:Review')->findByCorpus($row['Review']);

            // If not exist store new Review
            if(null == $stored_review)
            {
                $this->em->persist($review);
                $this->em->flush();
                $numNewReviews = $numNewReviews + 1;
            }

        }

        return array('reviews' => $reviews, 'numNewReviews' => $numNewReviews);
    }


    /**
     * Prepare review text
     *
     *  Skip specials characters and parse to lowercase
     *
     * @param $review
     * @return mixed|string
     */
    private function prepareReviewText($review)
    {
        // Skip special characters
        $review = preg_replace('/[^\w\s.,]/', ' ', $review);

        // Parse to lower case
        $review = strtolower($review);

        return $review;
    }

}