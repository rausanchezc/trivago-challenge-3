<?php
namespace AnalyzerBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use \Symfony\Component\Form\FormBuilderInterface;
use \Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TopicType extends AbstractType{

    /**
     * Form Builder
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Save'));

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event){

                $topic = $event->getData();
                $form = $event->getForm();

                $formOptions = array(
                    'class' => 'AnalyzerBundle\Entity\Topic',
                    'property' => 'name',
                    'label' => 'Category',
                    'placeholder' => 'None',
                    'required' => false,
                    'empty_data' => null,
                    'query_builder' => function (EntityRepository $er ){
                        return $er->createQueryBuilder('t')
                            ->where('t.mainKey IS NULL')
                            ->orderBy('t.name', 'ASC');
                    }
                );

                $form->add('mainKey', EntityType::class, $formOptions);
            }
        );
    }

    /**
     * Configure Options
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AnalyzerBundle\Entity\Topic'
        ));
    }


}