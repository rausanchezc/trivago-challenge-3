<?php

namespace AnalyzerBundle\Form\Type;


use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use \Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class QualifierType extends AbstractType{


    /**
     * Form builder
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',   TextType::class)
            ->add('weight', NumberType::class)
            ->add('type',   ChoiceType::class,
                array(
                'choices' => array(
                    'Positive' => 'POSITIVE',
                    'Negative' => 'NEGATIVE'
                ),
                'choices_as_values' => true,
            ))
            ->add('save', SubmitType::class, array('label' => 'Save'));

    }
}