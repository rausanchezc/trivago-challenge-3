<?php

namespace AnalyzerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Qualifier
 *
 * @ORM\Table(name="qualifier")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AnalyzerBundle\Repository\QualifierRepository")
 */
class Qualifier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="weight", type="float")
     */
    private $weight;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetime")
     */
    private $creationDate;

    /**
     * @ORM\OneToMany(targetEntity="Score", mappedBy="qualifier")
     */
    private $scores;

    /**
     * Qualifier constructor.
     */
    public function __construct()
    {
        $this->scores = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Qualifier
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Qualifier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return Qualifier
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set scores
     *
     * @param integer $scores
     * @return Qualifier
     */
    public function setScores($scores)
    {
        $this->scores = $scores;

        return $this;
    }

    /**
     * Get scores
     *
     * @return integer 
     */
    public function getScores()
    {
        return $this->scores;
    }

    /**
     * Add new score
     *
     * @param Score $score
     */
    public function addScore(Score $score)
    {
        $this->scores[] = $score;

    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Topic
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Remove score
     *
     * @param Score $score
     * @return ArrayCollection
     */
    public function removeScore(Score $score)
    {
        $this->scores->removeElement($score);

        return $this->scores;
    }

    /**
     * Set creation date before to persist
     *
     * @ORM\PrePersist
     */
    public function setCreationDateValue()
    {
        $this->creationDate = new \DateTime();
    }

    /**
     * To String
     *
     * @return string
     */
    function __toString()
    {
        return $this->getName();
    }


}
