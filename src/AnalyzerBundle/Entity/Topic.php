<?php

namespace AnalyzerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Topic
 *
 * @ORM\Table(name="topic")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AnalyzerBundle\Repository\TopicRepository")
 */
class Topic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetime")
     */
    private $creationDate;

    /**
     * @ORM\OneToMany(targetEntity="Topic", mappedBy="mainKey")
     */
    private $altKeys;

    /**
     * @ORM\ManyToOne(targetEntity="Topic", inversedBy="altKeys")
     */
    private $mainKey;

    /**
     * @ORM\OneToMany(targetEntity="Score", mappedBy="topic")
     */
    private $scores;


    /**
     * Topic constructor.
     */
    public function __construct()
    {
        $this->scores = new ArrayCollection();

        $this->altKeys = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Topic
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set mainKey
     *
     * @param Topic $mainKey
     * @return Topic
     */
    public function setMainKey(Topic $mainKey)
    {
        $this->mainKey = $mainKey;

        return $this;
    }

    /**
     * Get mainKey
     *
     * @return Topic
     */
    public function getMainKey()
    {
        return $this->mainKey;
    }

    /**
     * Get $altKeys
     *
     * @return ArrayCollection
     */
    public function getAltKeys()
    {
        return $this->altKeys;
    }

    /**
     * Set $altKeys
     *
     * @param ArrayCollection $altKeys
     */
    public function setAltKeys(ArrayCollection $altKeys)
    {
        $this->altKeys = $altKeys;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Topic
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set scores
     *
     * @param ArrayCollection $scores
     * @return Topic
     */
    public function setScores($scores)
    {
        $this->scores = $scores;

        return $this;
    }

    /**
     * Get scores
     *
     * @return ArrayCollection
     */
    public function getScores()
    {
        return $this->scores;
    }


    /**
     * Add new score
     *
     * @param Score $score
     */
    public function addScore(Score $score)
    {
        $this->scores[] = $score;

    }

    /**
     * Remove score
     *
     * @param Score $score
     * @return ArrayCollection
     */
    public function removeScore(Score $score)
    {
        $this->scores->removeElement($score);

        return $this->scores;
    }

    /**
     * Set creation date before to persist
     *
     * @ORM\PrePersist
     */
    public function setCreationDateValue()
    {
        $this->creationDate = new \DateTime();
    }


    /**
     * To String
     *
     * @return string
     */
    function __toString()
    {
        return $this->getName();
    }


}
