<?php

namespace AnalyzerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Review
 *
 * @ORM\Table(name="review")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AnalyzerBundle\Repository\ReviewRepository")
 */
class Review
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="corpus", type="string", length=1024)
     */
    private $corpus;

    /**
     * @var int
     *
     * @ORM\Column(name="hotelId", type="integer")
     */
    private $hotelId;

    /**
     * @var float
     *
     * @ORM\Column(name="totalScore", type="float", nullable=true)
     */
    private $totalScore;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastCalculationDate", type="datetime", nullable=true)
     */
    private $lastCalculationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creationDate", type="datetime")
     */
    private $creationDate;

    /**
     * @ORM\OneToMany(targetEntity="Score", mappedBy="review", cascade="remove")
     */
    private $scores;

    /**
     * Review constructor.
     */
    public function __construct()
    {
        $this->scores = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set corpus
     *
     * @param string $corpus
     * @return Review
     */
    public function setCorpus($corpus)
    {
        $this->corpus = $corpus;

        return $this;
    }

    /**
     * Get corpus
     *
     * @return string 
     */
    public function getCorpus()
    {
        return $this->corpus;
    }

    /**
     * Set hotelId
     *
     * @param integer $hotelId
     * @return Review
     */
    public function setHotelId($hotelId)
    {
        $this->hotelId = $hotelId;

        return $this;
    }

    /**
     * Get hotelId
     *
     * @return integer 
     */
    public function getHotelId()
    {
        return $this->hotelId;
    }

    /**
     * Set totalScore
     *
     * @param float $totalScore
     * @return Review
     */
    public function setTotalScore($totalScore)
    {
        $this->totalScore = $totalScore;

        return $this;
    }

    /**
     * Get totalScore
     *
     * @return float 
     */
    public function getTotalScore()
    {
        return $this->totalScore;
    }

    /**
     * Set lastCalculationDate
     *
     * @param \DateTime $lastCalculationDate
     * @return Review
     */
    public function setLastCalculationDate($lastCalculationDate)
    {
        $this->lastCalculationDate = $lastCalculationDate;

        return $this;
    }

    /**
     * Get lastCalculationDate
     *
     * @return \DateTime 
     */
    public function getLastCalculationDate()
    {
        return $this->lastCalculationDate;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Review
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set scores
     *
     * @param integer $scores
     * @return Review
     */
    public function setScores($scores)
    {
        $this->scores = $scores;

        return $this;
    }

    /**
     * Get scores
     *
     * @return ArrayCollection
     */
    public function getScores()
    {
        return $this->scores;
    }

    /**
     * Add new score
     *
     * @param Score $score
     */
    public function addScore(Score $score)
    {
        $this->scores[] = $score;

    }

    /**
     * Remove score
     *
     * @param Score $score
     * @return ArrayCollection
     */
    public function removeScore(Score $score)
    {
        $this->scores->removeElement($score);

        return $this->scores;
    }

    /**
     * Set creation date before to persist
     *
     * @ORM\PrePersist
     */
    public function setCreationDateValue()
    {
        $this->creationDate = new \DateTime();
    }
}
