<?php

namespace AnalyzerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Score
 *
 * @ORM\Table(name="score")
 * @ORM\Entity(repositoryClass="AnalyzerBundle\Repository\ScoreRepository")
 */
class Score
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Review", inversedBy="scores")
     */
    private $review;

    /**
     * @ORM\ManyToOne(targetEntity="Topic", inversedBy="scores")
     */
    private $topic;

    /**
     * @ORM\ManyToOne(targetEntity="Qualifier", inversedBy="scores")
     */
    private $qualifier;

    /**
     * @var int
     *
     * @ORM\Column(name="count", type="integer")
     */
    private $count;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set review
     *
     * @param Review $review
     * @return Score
     */
    public function setReview(Review $review)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return Review
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set topic
     *
     * @param Topic $topic
     * @return Score
     */
    public function setTopic(Topic $topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set qualifier
     *
     * @param Qualifier $qualifier
     * @return Score
     */
    public function setQualifier(Qualifier $qualifier)
    {
        $this->qualifier = $qualifier;

        return $this;
    }

    /**
     * Get qualifier
     *
     * @return Qualifier
     */
    public function getQualifier()
    {
        return $this->qualifier;
    }

    /**
     * Set count
     *
     * @param integer $count
     * @return Score
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return integer 
     */
    public function getCount()
    {
        return $this->count;
    }
}
