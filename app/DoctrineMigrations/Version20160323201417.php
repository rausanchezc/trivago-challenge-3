<?php

namespace TrivagoChallenge3\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160323201417 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE qualifier (id INTEGER NOT NULL, type VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, weight DOUBLE PRECISION NOT NULL, creationDate DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE review (id INTEGER NOT NULL, corpus VARCHAR(1024) NOT NULL, hotelId INTEGER NOT NULL, totalScore DOUBLE PRECISION DEFAULT NULL, lastCalculationDate DATETIME DEFAULT NULL, creationDate DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE score (id INTEGER NOT NULL, review_id INTEGER DEFAULT NULL, topic_id INTEGER DEFAULT NULL, qualifier_id INTEGER DEFAULT NULL, count INTEGER NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_329937513E2E969B ON score (review_id)');
        $this->addSql('CREATE INDEX IDX_329937511F55203D ON score (topic_id)');
        $this->addSql('CREATE INDEX IDX_32993751CEE0F86E ON score (qualifier_id)');
        $this->addSql('CREATE TABLE topic (id INTEGER NOT NULL, main_key_id INTEGER DEFAULT NULL, name VARCHAR(255) NOT NULL, creationDate DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9D40DE1B9208ED25 ON topic (main_key_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE qualifier');
        $this->addSql('DROP TABLE review');
        $this->addSql('DROP TABLE score');
        $this->addSql('DROP TABLE topic');
    }
}
